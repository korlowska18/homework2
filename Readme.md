# Przepis na **sernik babuni**

### Składniki

| GRAMATURA | SKŁADNIKI |
| --------- | --------- |
| 1,5 szkl. | mąka      |
| 0,75 szkl.| cukier    |
| 1kg       | ser       |
| 2 szt.    | jajko     |
| 2,5 szkl. | mleko     |
| 10 dag    | margaryna |

*Składniki zmiksować i piec w temperaturze __180 stopni__ przez godzinę*

## *Smacznego*
